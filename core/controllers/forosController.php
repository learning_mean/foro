<?php
if(isset($_GET['id']) && is_numeric($_GET['id']) && $_GET['id'] >= 1) {
 // echo 'Estas en controlador de foros con una id ' . $_GET['id'] . ' y su tipo ' . gettype($_GET['id']) ;
  $id_foro = intval($_GET['id']);
  if(array_key_exists($id_foro, $fora)) {
	  $db = new Conexion();
	  $query = 'SELECT * FROM temas WHERE id_foro = :id_foro AND tipo = 2 ORDER BY id DESC';
	  $stmnt = $db->prepare($query);
	  $stmnt->bindValue(':id_foro', $id_foro);
	  $stmnt->execute();
	  $resultAnuncios =  $stmnt->fetchAll();
	  $stmnt->closeCursor();
	  $query = 'SELECT * FROM temas WHERE id_foro = :id_foro AND tipo = 1 ORDER BY id DESC';
	  $stmnt = $db->prepare($query);
	  $stmnt->bindValue(':id_foro', $id_foro);
	  $stmnt->execute();
	  $resultTemas =  $stmnt->fetchAll();
	  $stmnt->closeCursor();
	  include(HTML_DIR. 'temas/temas.php');
  } else {
	  header('location: ../index.php?view=error');
  }
} else {
  header('location: index.php?view=index');
}




 ?>
