<?php

if (isset($_GET['key']) && !isset($_SESSION['app_id'])) {
    $keypass = filter_input(INPUT_GET, 'key', FILTER_SANITIZE_STRING);
    $db = new Conexion();
    $stmnt = $db->prepare('SELECT id, newpass FROM users WHERE keypass = :keypass');
    $stmnt->bindValue(':keypass', $keypass);
    $stmnt->execute();
    $result = $stmnt->fetch();
    $stmnt->closeCursor();
    if($result) {
        $prenewpass = $result['newpass'];
        $newpass = Encrypt($prenewpass);
        $id = $result['id'];
        $nokey = "";
        $nopass = "";
        $stmnt = $db->prepare('UPDATE users SET keypass = :nokey, newpass = :nopass, pass = :newpass WHERE id = :id');
        $stmnt->bindValue(':newpass', $newpass);
        $stmnt->bindValue(':id', $id);
        $stmnt->bindValue(':nokey', $nokey);
        $stmnt->bindValue(':nopass', $nopass);
        $stmnt->execute();
        $stmnt->closeCursor();
        include(HTML_DIR . 'lostpass_mensaje.php');
    } else {
        header('location: ?view=index');
    }
} else {
    header('location: ?view=index');
}

 ?>
