<?php
if(isset($_SESSION['app_id']) && $users[$_SESSION['app_id']]['permisos'] == 2) {
    $isset_id = isset($_GET['id']) && is_numeric($_GET['id']) && $_GET['id'] >=1;
    require('core/models/class.ConfigForos.php');
    $configForos = new ConfigForos();
    switch (isset($_GET['mode']) ? $_GET['mode'] : null) {
        case 'add':
            if($_POST) {
               $configForos->add();
            } else {
                include(HTML_DIR . 'foros/add_foros.php' );
            }
            break;
        case 'edit':
            if ($isset_id) {
                if($_POST) {
                  if (array_key_exists(intval($_GET['id']), $fora)) {
                    $configForos->edit();
                  } else {
                    header('location: ?view=configforos');
                  }

            } else {
                include(HTML_DIR . 'foros/edit_foros.php' );
            }

            } else {
                header('location: ?view=configforos');
            }
            break;
        case 'delete':
            if ($isset_id) {
              $configForos->delete();
            } else {
                header('location: ?view=configforos');
            }
            break;
        default:
            include(HTML_DIR . 'foros/all_foros.php' );
            break;
    }
} else {
    header('location: ?view=index');
}

?>
