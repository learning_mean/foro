<?php
if(isset($_SESSION['app_id']) && $users[$_SESSION['app_id']]['permisos'] == 2) {
    $isset_id = isset($_GET['id']) && is_numeric($_GET['id']) && $_GET['id'] >=1;
    require('core/models/class.Categorias.php');
    $categorias = new Categorias();
    switch (isset($_GET['mode']) ? $_GET['mode'] : null) {
        case 'add':
            if($_POST) {
               $categorias->add();
            } else {
                include(HTML_DIR . 'categorias/add_categorias.php' );
            }
            break;
        case 'edit':
            if ($isset_id) {
                if($_POST) {
                  if (array_key_exists(intval($_GET['id']), $categories)) {
                    $categorias->edit();
                  } else {
                    header('location: ?view=categorias');
                  }

            } else {
                include(HTML_DIR . 'categorias/edit_categorias.php' );
            }

            } else {
                header('location: ?view=categorias');
            }
            break;
        case 'delete':
            if ($isset_id) {
              $categorias->delete();
            } else {
                header('location: ?view=categorias');
            }
            break;
        default:
            include(HTML_DIR . 'categorias/all_categorias.php' );
            break;
    }
} else {
    header('location: ?view=index');
}

?>
