<?php

if(isset($_GET['key'], $_SESSION['app_id'])) {
    $key = filter_input(INPUT_GET, 'key', FILTER_SANITIZE_STRING);
    $db = new Conexion();
    $stmnt = $db->prepare("SELECT * FROM users WHERE keyreg = :key AND id = :id");
    $stmnt->bindValue(':key', $key);
    $stmnt->bindValue(':id', $_SESSION['app_id']);
    $stmnt->execute();
    $result = $stmnt->fetch();
    $stmnt->closeCursor();
    if($result) {
        $query = "UPDATE users SET activado = '1', keyreg = '' WHERE id = :id";
        $stmnt = $db->prepare($query);
        $stmnt->bindValue(':id', $_SESSION['app_id']);
        $stmnt->execute();
        $stmnt->closeCursor();
        header('location: ?view=index&success=true?id='.$ii);
    } else {
        header('location: ?view=index&error=true');
    }
    
} else {
    include(HTML_DIR .'public/loguearte.php');
}

?>

