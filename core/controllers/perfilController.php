<?php
if(isset($_GET['id']) && array_key_exists($_GET['id'], $users)) {
  $id_usuario = intval($_GET['id']);
    $db = new Conexion();
    $query = 'SELECT COUNT(*) FROM temas WHERE id_creador = :id_creador';
    $stmnt = $db->prepare($query);
    $stmnt->bindValue(':id_creador', $id_usuario);
    $stmnt->execute();
    $num_temas = $stmnt->fetchColumn();
    $stmnt->closeCursor();
  include(HTML_DIR . 'perfil/perfil.php');
} else {
  header('location: ?view=index');
}


?>
