<?php
session_start();
error_reporting(0);
//views/app contiene el css y el js de la página independiente del modelo mvc
//html contiene las plantillas html de las páginas
//core/bin/ajax contiene los archivos php que son llamados a través de ajax
//core es el núcleo de toda la aplicación contiene las carpetas de modelo mvc
#Este archivo es el núcleo de la aplicación, desde aquí vamos a hacer llamada
#a todos los archivos que vaya a necesitar la aplicación.

//Constantes de la Conexion
define('DB_HOST', 'localhost');
define('DB_USER', 'prinick');
define('DB_PASS', '123456');
define('DB_NAME', 'foro');
define('DSN','mysql:host=localhost;dbname=foro;charset=utf8');
//Constantes phpmailer
define('MAIL_HOST','smtp.gmail.com');
define('MAIL_USER','traficositioweb@gmail.com');
define('MAIL_PASS','tttttttttt11');
define('MAIL_PROTOCOL','tls');
define('MAIL_PORT', 587);
//Constantes de la app
define('HTML_DIR', 'html/');
define('APP_TITLE', 'ForosASIR');
define('APP_COPY', 'Copyright &copy; ' . date('Y',time()) . ' ForosASIR Software.');
define('APP_URL', 'http://172.16.3.253');
//Constantes básicas de personalización
define('MIN_LONGITUD_TITULO_TEMA', 5);
define('MIN_LONGITUD_CONTENIDO_TEMA', 275);
//Zona horaria
date_default_timezone_set('Europe/Madrid');
//Estructura
require('vendor/autoload.php');
require('core/models/class.Conexion.php');
require('core/bin/functions/Encrypt.php');
require('core/bin/functions/Users.php');
require('core/bin/functions/EmailTemplate.php');
require('core/bin/functions/LostpassTemplate.php');
require('core/bin/functions/SendMail.php');
require('core/bin/functions/Categorias.php');
require('core/bin/functions/Foros.php');
require('core/bin/functions/Temas.php');
require('core/bin/functions/UrlAmigable.php');
require('core/bin/functions/BBcode.php');
require('core/bin/functions/OnlineUsers.php');
require('core/bin/functions/GetUserStatus.php');
require('core/bin/functions/IncrementarVisitas.php');

$users = Users(); //recupera los usuarios logueados
$categories = Categorias(); //recupera todas las categorias
$fora = Foros(); //recupera todos los foros
$temario = Temas(); //recupera todos los temas

 ?>
