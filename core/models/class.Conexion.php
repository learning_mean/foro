<?php
class Conexion extends PDO {

  public function __construct() {
    parent::__construct(DSN, DB_USER, DB_PASS);
    $this->setAttribute(parent::ATTR_ERRMODE, parent::ERRMODE_EXCEPTION);
  }

  public function recorrer($sql) {
      $db = $this;
      $statement = $db->query($sql);
      $resultados = $statement->fetchAll();
      $statement->closeCursor();
      if($resultados) {
        return $resultados;
      } else {
        return false;
      }     

  }

}








?>
