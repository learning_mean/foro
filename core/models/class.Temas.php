<?php

class Temas {
	private $db;
	private $id;
  private $titulo;
	private $contenido;
	private $id_foro;
	private $id_creador;
	private $estado;
	private $tipo;
	private $visitas;
	private $respuestas;
	private $id_ultimo;
	private $fecha_ultimo;
	private $anuncio;

	public function __construct() {
		$this->db = new Conexion();
		$this->id = isset($_GET['id']) ? intval($_GET['id']) : NULL;
		$this->id_foro = intval($_GET['id_foro']);
		$this->id_creador = isset($_SESSION['app_id'])  ?  $_SESSION['app_id'] : NULL;
	}

	private function errors($url, $add_mode = false) {
    try {
      if (empty($_POST['titulo']) || empty($_POST['contenido'])) {
        throw new Exception(1);
      } else {
        $this->titulo = filter_input(INPUT_POST, 'titulo', FILTER_SANITIZE_STRING);
        $this->contenido = filter_input(INPUT_POST, 'contenido', FILTER_SANITIZE_STRING);
      }
      if(strlen($this->titulo) < MIN_LONGITUD_TITULO_TEMA) {
        throw new Exception(2);
      }
	  	if(strlen($this->contenido) < MIN_LONGITUD_CONTENIDO_TEMA) {
        throw new Exception(3);
      }
			if (isset($_POST['anuncio']) && $_POST['anuncio'] == 1) {
				$this->anuncio = 2;
			} else {
				$this->anuncio = 1;
			}


    } catch (Exception $e) {
      header('location: ' . $url . $e->getMessage());
      exit;
    }
}

public function add() {
    $this->errors('?view=temas&mode=add&id_foro=' . $this->id_foro . '&error=', true);
	  $fecha = date('d/m/Y h:i a', time());
    $query = 'INSERT INTO temas (titulo, contenido, fecha, id_creador, id_foro, id_ultimo, fecha_ultimo, tipo) VALUES (:titulo, :contenido, :fecha, :id_creador, :id_foro, :id_ultimo, :fecha_ultimo, :tipo)';
    $stmnt = $this->db->prepare($query);
    $stmnt->bindValue(':titulo', $this->titulo);
    $stmnt->bindValue(':contenido', $this->contenido);
		$stmnt->bindValue(':fecha', $fecha);
		$stmnt->bindValue(':id_creador', $this->id_creador);
		$stmnt->bindValue(':id_foro', $this->id_foro);
		$stmnt->bindValue(':id_ultimo', $this->id_creador);
		$stmnt->bindValue(':fecha_ultimo', $fecha);
		$stmnt->bindValue(':tipo', $this->anuncio);
    $stmnt->execute();
		$idTema = $this->db->lastInsertId();
    $stmnt->closeCursor();
		//hemos de modificar 2 campos del foro al insertar un nuevo Tema
		$query = 'UPDATE foros SET numero_temas = numero_temas + 1, numero_mensajes = numero_mensajes + 1, ultimo_tema= :ultimoTema, id_ultimo_tema= :idUltimoTema WHERE id = :id;';
		$query .= 'UPDATE users SET mensajes = mensajes + 1 WHERE id = :id_creador';
		$stmnt = $this->db->prepare($query);
		$stmnt->bindValue(':id', $this->id_foro);
		$stmnt->bindValue(':ultimoTema', $this->titulo);
		$stmnt->bindValue(':idUltimoTema', $idTema);
		$stmnt->bindValue(':id_creador', $this->id_creador);
		$stmnt->execute();
    $stmnt->closeCursor();
		//$this->updateMisMensajes();
    header('location: temas/' . UrlAmigable($idTema, $this->titulo, $this->id_foro));

}

public function edit() {
    $this->errors('?view=temas&mode=edit&id=' . $this->id . '&id_foro=' . $this->id_foro . '&error=');
    $query = 'UPDATE temas SET titulo = :titulo, contenido = :contenido,  tipo = :tipo WHERE id = :id';
    $stmnt = $this->db->prepare($query);
		$stmnt->bindValue(':id', $this->id);
   	$stmnt->bindValue(':titulo', $this->titulo);
    $stmnt->bindValue(':contenido', $this->contenido);
    //$stmnt->bindValue(':id_foro', $this->id_foro);
    //$stmnt->bindValue(':id_creador', $this->id_creador);
		//$stmnt->bindValue(':estado', $this->estado);
    $stmnt->bindValue(':tipo', $this->anuncio);
    //$stmnt->bindValue(':fecha', $this->fecha);
    //$stmnt->bindValue(':visitas', $this->visitas);
		//$stmnt->bindValue(':respuestas', $this->respuestas);
    //$stmnt->bindValue(':id_ultimo', $this->id_ultimo);
    //$stmnt->bindValue(':fecha_ultimo', $this->fecha_ultimo);
    $stmnt->execute();
    $stmnt->closeCursor();
    header('location: temas/' . UrlAmigable($this->id, $this->titulo, $this->id_foro));
}

public function delete() {
		$id_tema = $this->id;
		$id_foro = $this->id_foro;
		############################
		$q = 'SELECT id_creador from respuestas WHERE id_tema = ' . $this->id;
		$stmnt = $this->db->query($q);
		if ($stmnt) {
			foreach($stmnt as $row) {
				$this->db->query("UPDATE users SET mensajes = mensajes -1 WHERE id='$row[id_creador]';");
			}
		}
		$stmnt->closeCursor();
		##########################
    $query = 'DELETE FROM temas WHERE id = ?;';
		$query .= 'UPDATE foros SET numero_temas = numero_temas - 1, numero_mensajes = numero_mensajes - 1 WHERE id = ?;';
		//$query .= 'DELETE FROM respuestas WHERE id_tema = ?;';
    $stmnt = $this->db->prepare($query);
    $stmnt->bindParam(1, $id_tema, PDO::PARAM_INT);
		$stmnt->bindParam(2, $id_foro, PDO::PARAM_INT);
		//$stmnt->bindParam(3, $id_tema, PDO::PARAM_INT);
    $stmnt->execute();
    $stmnt->closeCursor();
		$query = "DELETE FROM respuestas WHERE id_tema = {$id_tema};";
		$resultado = $this->db->query($query);
		$mensajesBorrados = $resultado->rowCount();
		$resultado->closeCursor();
		$q = "UPDATE foros SET numero_mensajes = numero_mensajes - {$mensajesBorrados} WHERE id = {$id_foro}";
		$this->db->query($q);
		#########################Trabajando en esta seccion
		$stmnt = $this->db->query("SELECT max(id) FROM temas WHERE id_foro = $this->id_foro");
		$idTema = $stmnt->fetchColumn();
		if(NULL == $idTema || !isset($idTema)) {
			$q = "UPDATE foros SET ultimo_tema = '', id_ultimo_tema = NULL WHERE id = $this->id_foro;";
			$stmnt = $this->db->query($q);
			$stmnt->closeCursor();
	    header('location: ?view=foros&id=' . $this->id_foro);
		}

		$res = $this->db->recorrer("SELECT * FROM temas WHERE id ='$idTema'");
		$q = "UPDATE foros SET ultimo_tema = '{$res[0]['titulo']}', id_ultimo_tema = '{$res[0]['id']}' WHERE id = $this->id_foro;";
		$stmnt = $this->db->query($q);
		$stmnt->closeCursor();
    header('location: ?view=foros&id=' . $this->id_foro);
}

public function mostrar() {
	if (isset($this->id)) {
		$query = 'SELECT * FROM temas WHERE id = :id LIMIT 1';
    $stmnt = $this->db->prepare($query);
		$stmnt->bindValue(':id', $this->id);
		$stmnt->execute();
    $result = $stmnt->fetch();
    $stmnt->closeCursor();
		if($result) {
			return $result;
		} else {
			return false;
		}
	} else {
		$query = 'SELECT * FROM temas';
    $stmnt = $this->db->prepare($query);
		$stmnt->execute();
    $result = $stmnt->fetchAll();
    $stmnt->closeCursor();
		if($result) {
			return $result;
		} else {
			return false;
		}

	}

}

public function close(int $estado) {
    //$this->errors('?view=temas&mode=edit&id=' . $this->id .'&error=');
		$estado = intval($estado);
    $query = 'UPDATE temas SET estado = :estado WHERE id = :id';
    $stmnt = $this->db->prepare($query);
   	$stmnt->bindValue(':id', $this->id);
	  $stmnt->bindValue(':estado', $estado);
    $stmnt->execute();
    $stmnt->closeCursor();
    header('location: ?view=temas&id=' . $this->id .'&id_foro=' . $this->id_foro);
}

public function anuncio() {
	$this->errors('?view=temas&mode=edit&id=' . $this->id .'&error=');
    $query = 'UPDATE temas SET tipo = :tipo WHERE id = :id';
    $stmnt = $this->db->prepare($query);
   	$stmnt->bindValue(':id', $this->id);
	$stmnt->bindValue(':tipo', $this->tipo);
    $stmnt->execute();
    $stmnt->closeCursor();
    header('location: ?view=temas&mode=edit&id=' . $this->id .'&success=true');
}

public function responder() {
	if (empty($_POST['contenido'])) {
		header('location: ?view=temas&mode=responder&id=' . $this->id .'&id_foro=' . $this->id_foro);
		exit;
	} else {
		$this->contenido = filter_input(INPUT_POST, 'contenido', FILTER_SANITIZE_STRING);
	}
	$query = 'INSERT INTO respuestas (id_creador, id_tema, id_foro, contenido) VALUES (:id_creador, :id_tema, :id_foro, :contenido)';
	$stmnt = $this->db->prepare($query);
	$stmnt->bindValue(':id_creador', $this->id_creador);
	$stmnt->bindValue(':id_tema', $this->id);
	$stmnt->bindValue(':id_foro', $this->id_foro);
	$stmnt->bindValue(':contenido', $this->contenido);
	$stmnt->execute();
	$stmnt->closeCursor();
	$query = 'UPDATE foros SET numero_mensajes = numero_mensajes + 1 WHERE id = :id;';
	$query .= 'UPDATE users SET mensajes = mensajes + 1 WHERE id = :id_creador;';
	$query .= 'UPDATE temas SET respuestas = respuestas + 1 WHERE id = :id_asunto;';
	$stmnt = $this->db->prepare($query);
	$stmnt->bindValue(':id', $this->id_foro);
	$stmnt->bindValue(':id_creador', $this->id_creador);
	$stmnt->bindValue(':id_asunto', $this->id);
	$stmnt->execute();
	$stmnt->closeCursor();
	//$this->updateMisMensajes();
	header('location: index.php?view=temas&id='. $this->id .'&id_foro=' . $this->id_foro);
}

private function updateMisMensajes() {
	$_SESSION['users'][$this->id_creador]['mensajes']++;
}

public function getRespuestas() {
	$query = 'SELECT * FROM respuestas WHERE id_tema = :id';
	$stmnt = $this->db->prepare($query);
	$stmnt->bindValue(':id', $this->id);
	$stmnt->execute();
	$result = $stmnt->fetchAll();
	$stmnt->closeCursor();
	if($result) {
		return $result;
	} else {
		return false;
	}
}

}
