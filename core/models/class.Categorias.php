<?php

class Categorias {
private $db;
private $id;
private $nombre;

public function __construct() {
    $this->db = new Conexion();
}

private function errors($url) {
    try {
      if (empty($_POST['nombre'])) {
        throw new Exception("La variable nombre está vacía", 1);
      } else {
        $this->nombre = filter_input(INPUT_POST, 'nombre', FILTER_SANITIZE_STRING);
      }
    } catch (Exception $e) {
      header('location: ' . $url . $e->getMessage());
      exit;
    }


}

public function add() {
    $this->errors('?view=categorias&mode=add&error=');
    $query = 'INSERT INTO categorias (nombre) VALUES (:nombre)';
    $stmnt = $this->db->prepare($query);
    $stmnt->bindValue(':nombre', $this->nombre);
    $stmnt->execute();
    $stmnt->closeCursor();
    header('location: ?view=categorias&mode=add&id=' . $this->id .'&success=true');
}

public function edit() {
    $this->id = intval($_GET['id']);
    $this->errors('?view=categorias&mode=edit&id=' . $this->id .'&error=');
    $query = 'UPDATE categorias SET nombre = :nombre WHERE id = :id';
    $stmnt = $this->db->prepare($query);
    $stmnt->bindValue(':nombre', $this->nombre);
    $stmnt->bindValue(':id', $this->id);
    $stmnt->execute();
    $stmnt->closeCursor();
    header('location: ?view=categorias&mode=edit&id=' . $this->id .'&success=true');
}

public function delete() {
  include('core/models/class.ConfigForos.php');
    $this->id = intval($_GET['id']);
    $query = 'SELECT id FROM foros WHERE id_categoria=:id';
    $stmnt = $this->db->prepare($query);
    $stmnt->bindValue(':id', $this->id);
    $stmnt->execute();
    $result = $stmnt->fetchAll();
    $stmnt->closeCursor();
    if($result) {
      $foro = new ConfigForos();
      foreach ($result as $row) {
        $foro->delete($row['id']);
      }
    }

  //  $query = 'DELETE FROM categorias WHERE id = :id;
  //            DELETE FROM foros WHERE id_categoria = :id';
  $query = 'DELETE FROM categorias WHERE id = :id';
    $stmnt = $this->db->prepare($query);
    $stmnt->bindValue(':id', $this->id);
    $stmnt->execute();
    $stmnt->closeCursor();
    header('location: ?view=categorias');
}

public function mostrar() {
    $query = 'SELECT * FROM categorias';
    $stmnt = $this->db->prepare($query);
    $stmnt->execute();
    $result = $stmnt->fetchAll();
    $stmnt->closeCursor();
    return $result;
}

}




?>
