<?php

class ConfigForos {
private $db;
private $id;
private $nombre;
private $descripcion;
private $numeroMensajes;
private $numeroTemas;
private $idCategoria;
private $estado;


public function __construct() {
    $this->db = new Conexion();
}

private function errors($url, $add_mode = false) {
  global $categories;
    try {
      if (empty($_POST['nombre']) || empty($_POST['descripcion']) || !isset($_POST['estado'])) {
        throw new Exception(1);
      } else {
        $this->nombre = filter_input(INPUT_POST, 'nombre', FILTER_SANITIZE_STRING);
        $this->descripcion = filter_input(INPUT_POST, 'descripcion', FILTER_SANITIZE_STRING);
        $search = array('<script>', '</script>', 'script', '<script src');
        $this->descripcion = str_ireplace($search, '', $this->descripcion);
        $this->estado = (int) filter_input(INPUT_POST, 'estado');
      }
      if(array_key_exists($_POST['categoria'], $categories)) {
        $this->idCategoria = intval(filter_input(INPUT_POST, 'categoria'));
      } else {
        throw new Exception(2);
      }
    } catch (Exception $e) {
      header('location: ' . $url . $e->getMessage());
      exit;
    }


}

public function add() {
    $this->errors('?view=configforos&mode=add&error=', true);
    $query = 'INSERT INTO foros (nombre, descripcion, id_categoria, estado) VALUES (:nombre, :descripcion, :categoria, :estado)';
    $stmnt = $this->db->prepare($query);
    $stmnt->bindValue(':nombre', $this->nombre);
    $stmnt->bindValue(':descripcion', $this->descripcion);
    $stmnt->bindValue(':categoria', $this->idCategoria);
    $stmnt->bindValue(':estado', $this->estado);
    $stmnt->execute();
    $stmnt->closeCursor();
    header('location: ?view=configforos&mode=add&id=' . $this->id .'&success=true');
}

public function edit() {
    $this->id = intval($_GET['id']);
    $this->errors('?view=configforos&mode=edit&id=' . $this->id .'&error=');
    $query = 'UPDATE foros SET nombre = :nombre, descripcion = :descripcion, id_categoria = :categoria, estado = :estado WHERE id = :id';
    $stmnt = $this->db->prepare($query);
    $stmnt->bindValue(':nombre', $this->nombre);
    $stmnt->bindValue(':id', $this->id);
    $stmnt->bindValue(':descripcion', $this->descripcion);
    $stmnt->bindValue(':categoria', $this->idCategoria);
    $stmnt->bindValue(':estado', $this->estado);
    $stmnt->execute();
    $stmnt->closeCursor();
    header('location: ?view=configforos&mode=edit&id=' . $this->id .'&success=true');
}

/*public function delete() {
    $this->id = intval($_GET['id']);
    $query = 'DELETE FROM foros WHERE id = :id;';
    $query .= 'DELETE FROM temas WHERE id_foro = :id;';
    $stmnt = $this->db->prepare($query);
    $stmnt->bindValue(':id', $this->id);
    $stmnt->execute();
    $stmnt->closeCursor();
    header('location: ?view=configforos&success=true');
}*/
public function delete($cat=-1) {
    $this->id = intval($_GET['id']);
    $query = 'DELETE FROM foros WHERE id = :id;';
    $query .= 'DELETE FROM temas WHERE id_foro = :id;';
    $stmnt = $this->db->prepare($query);
    if($cat!=-1) {
      $stmnt->bindValue(':id', $cat);
    } else {
      $stmnt->bindValue(':id', $this->id);
    }
    $stmnt->execute();
    $stmnt->closeCursor();
    if($cat!=-1) {
      return true;
    }
    header('location: ?view=configforos&success=true');
}

public function mostrar() {
    $query = 'SELECT * FROM foros';
    $stmnt = $this->db->prepare($query);
    $stmnt->execute();
    $result = $stmnt->fetchAll();
    $stmnt->closeCursor();
    return $result;
}

}




?>
