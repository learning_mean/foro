<?php

$db = new Conexion();
$user = filter_input(INPUT_POST, trim('user'),FILTER_SANITIZE_STRING);
$pass = encrypt(filter_input(INPUT_POST, trim('pass'), FILTER_SANITIZE_STRING));
$query = "SELECT id FROM users WHERE (user = :user OR email = :user) AND pass = :pass";
$stmnt = $db->prepare($query);
$stmnt->bindValue(':user', $user);
$stmnt->bindValue(':pass', $pass);
$stmnt->execute();
$result = $stmnt->fetch();
$stmnt->closeCursor();
if($result) {
  if ($_POST['sesion'] === true) {
    ini_set('session_cookie_lifetime', time() + 60*60*24);
  }
  $_SESSION['app_id'] = $result['id'];
  $_SESSION['time_online'] = time() - (60*6);
  $html = (int)1;
} else {
  $html = '<div class="alert alert-dismissible alert-danger">
          <button type="button" class="close" data-dismiss="alert">x</button>
          <h4>Login Error</h4>
          <p><strong>Verifica tus datos</strong></p>
        </div>';
}
echo $html;
 ?>
