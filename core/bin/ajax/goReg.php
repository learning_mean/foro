<?php

    $user = filter_input(INPUT_POST, trim('user'), FILTER_SANITIZE_STRING);
    $pass = Encrypt(filter_input(INPUT_POST, trim('pass'), FILTER_SANITIZE_STRING));
    $email = filter_input(INPUT_POST, trim('email'), FILTER_SANITIZE_EMAIL);
    if ($user && $pass && $email) {
        $db = new Conexion();
        $query = "SELECT * FROM users WHERE user = :user OR email = :email";
        $statement = $db->prepare($query);
        $statement->bindValue(':user', $user);
        $statement->bindValue(':email', $email);
        $statement->execute();
        $result = $statement->fetch();
        $statement->closeCursor();
        if($result) {
            $nombre = $db->recorrer("SELECT * FROM users WHERE user='$user' OR email='$email' LIMIT 1");
            if ($result['user'] == $nombre[0]['user'] ) {
                $html = '<div class="alert alert-dismissible alert-danger">
                        <button type="button" class="close" data-dismiss="alert">&times;</button>
                        <strong>Nombre de Usuario</strong> ya registrado.</div>';
            } else {
               $html = '<div class="alert alert-dismissible alert-danger">
                        <button type="button" class="close" data-dismiss="alert">&times;</button>
                        <strong>Dirección Email</strong> ya registrado.</div>';
            }
        } else {
            $keyreg = md5(time());
            $link = APP_URL . '?view=activar&key=' . $keyreg;
            $isSent = sendEmail($email, $user, $link);
            if(!$isSent) {
                $html = '<div class="alert alert-dismissible alert-danger">
                        <button type="button" class="close" data-dismiss="alert">&times;</button>
                        <strong>El email no pudo enviarse</strong></div>';
            } else {
                $query = "INSERT INTO users (user, email, pass, keyreg, fecha_reg) ";
                $query .= "VALUES (:user, :email, :pass, :keyreg, :fecha_reg);";
                $fechaReg = date('d/m/Y', time());
                $statement = $db->prepare($query);
                $statement->bindValue(':user', $user);
                $statement->bindValue(':email', $email);
                $statement->bindValue(':pass', $pass);
                $statement->bindValue(':keyreg', $keyreg);
                $statement->bindValue(':fecha_reg', $fechaReg);
                $statement->execute();
                $statement->closeCursor();
                $_SESSION['app_id'] = $db->lastInsertId();//inicio la sesion para el usuario recien registrado
                $html = (int)1;
            }

        }
    }
echo $html;
 ?>
