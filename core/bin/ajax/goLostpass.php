<?php
$email = filter_input(INPUT_POST, 'email', FILTER_SANITIZE_EMAIL);
$db = new Conexion();
$stmnt = $db->prepare('SELECT id, user, email from users WHERE email = :email');
$stmnt->bindValue(':email', $email);
$stmnt->execute();
$result = $stmnt->fetch();
$stmnt->closeCursor();
if ($result) {
  $keypass = md5(time());
  $newpass = strtoupper(substr(sha1(time()), 1, 8));
  $link = APP_URL . '?view=lostpass&key=' . $keypass;
  $isSent = sendEmail($result['email'], $result['user'], $link, true);
  if ($isSent) {
    $query = 'UPDATE users SET keypass = :keypass, newpass = :newpass WHERE id = :id';
    $stmnt = $db->prepare($query);
    $stmnt->bindValue(':keypass', $keypass);
    $stmnt->bindValue(':newpass', $newpass);
    $stmnt->bindValue(':id', $result['id']);
    $stmnt->execute();
    $stmnt->closeCursor();
    $html = (int) 1;
  } else {
      $html = '<div class="alert alert-dismissible alert-danger">
      <button type="button" class="close" data-dismiss="alert">&times;</button>
      <h4>¡Error!</h4><p>Se ha producido un fallo en la recuperación de tu contraseña</p></div>';
  }

} else {
  $html = '<div class="alert alert-dismissible alert-danger">
  <button type="button" class="close" data-dismiss="alert">&times;</button>
  <h4>¡Email no encontrado!</h4><p>El email proporcionado no está registrado</p></div>';
}


 echo $html;
?>
