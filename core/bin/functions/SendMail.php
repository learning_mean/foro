<?php

function sendEmail($email, $user, $link, $recuperacion = false) {
  $mail = new PHPMailer;

  //$mail->SMTPDebug = 3;                               // Enable verbose debug output
  $mail->Charset = "UTF-8";  //define el conjunto de caracteres usados
  $mail->Encoding = "quoted-printable";
  $mail->isSMTP();                                      // Set mailer to use SMTP
  $mail->Host = MAIL_HOST;  // Specify main and backup SMTP servers
  $mail->SMTPAuth = true;                               // Enable SMTP authentication
  $mail->Username = MAIL_USER;                 // SMTP username
  $mail->Password = MAIL_PASS;                           // SMTP password
  $mail->SMTPSecure = MAIL_PROTOCOL;                            // Enable TLS encryption, `ssl` also accepted
  $mail->Port = MAIL_PORT;                                    // TCP port to connect to

  $mail->setFrom(MAIL_USER, APP_TITLE);//Quién envia el correo
  $mail->addAddress($email, $user);// Destinatario del correo
 // $mail->addAddress('ellen@example.com');               // Name is optional
  //$mail->addReplyTo('info@example.com', 'Information');
  //$mail->addCC('cc@example.com');
 // $mail->addBCC('bcc@example.com');

 // $mail->addAttachment('/var/tmp/file.tar.gz');         // Add attachments
  //$mail->addAttachment('/tmp/image.jpg', 'new.jpg');    // Optional name
  $mail->isHTML(true);                                  // Set email format to HTML

  $mail->Subject = 'Atención al cliente ' . APP_TITLE;
  $mail->Body    = $recuperacion ? LostpassTemplate($user, $link) : EmailTemplate($user, $link);
  $mail->AltBody = 'Estimado ' . $user . '. Por favor siga este enlace. '
          . $link;

  if(!$mail->send()) {
      return false;
  } else {
      return true;
  }
}


 ?>
