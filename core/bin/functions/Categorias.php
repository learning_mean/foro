<?php
function Categorias() {
  $db = new Conexion();
  $query = 'SELECT * FROM categorias;';
  $stmnt = $db->prepare($query);
  $stmnt->execute();
  $result = $stmnt->fetchAll();
  $stmnt->closeCursor();
  if ($result) {
    foreach ($result as $row) {
      $categorias[$row['id']] = $row;
    }

  } else {
    $categorias = false;
  }
  return $categorias;
}
 ?>
