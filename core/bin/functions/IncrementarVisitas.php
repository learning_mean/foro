<?php
function IncrementarVisitas(int $id) {
  $db = new Conexion();
  $query = 'UPDATE temas SET visitas = visitas + 1 WHERE id = :id LIMIT 1';
  $stmnt = $db->prepare($query);
  $stmnt->bindValue(':id', $id);
  $stmnt->execute();
  $stmnt->closeCursor();
}


 ?>
