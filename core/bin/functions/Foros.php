<?php
function Foros() {
  $db = new Conexion();
  $query = 'SELECT * FROM foros;';
  $stmnt = $db->prepare($query);
  $stmnt->execute();
  $result = $stmnt->fetchAll();
  $stmnt->closeCursor();
  if ($result) {
    foreach ($result as $row) {
      $foros[$row['id']] = $row;
    }

  } else {
    $foros = false;
  }
  return $foros;
}
 ?>
