<?php

function LostpassTemplate($user,$link) {
  $HTML = '
  <html>
  <body style="background: #FFFFFF;font-family: Verdana; font-size: 14px;color:#1c1b1b;">
  <div style="">
      <h2>Hola '.$user.'</h2>
      <p style="font-size:17px;">Solicitud de recuperacion de contrase&nacute;a '. APP_TITLE .'.</p>
  	<p>Hemos recibido una solicitud de cambio de contrasena el ' . date('d/m/Y', time()) .
    '. Sigue el enlace y logueate con la nueva contrasena que se te proporcionara¡.</p>
  	<p style="padding:15px;background-color:#ECF8FF;">
            Para recuperar tu contrasena por favor haz <a style="font-weight:bold;color: #2BA6CB;" href="'.$link.'" target="_blank">clic aqui &raquo;</a>
  	</p>
      <p style="font-size: 9px;">&copy; '. date('Y',time()) .' '.APP_TITLE.'. Todos los derechos reservados.</p>
  </div>
  </body>
  </html>
  ';
      return $HTML;
}

?>
