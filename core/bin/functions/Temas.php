<?php
function Temas() {
  $db = new Conexion();
  $query = 'SELECT * FROM temas;';
  $stmnt = $db->prepare($query);
  $stmnt->execute();
  $result = $stmnt->fetchAll();
  $stmnt->closeCursor();
  if ($result) {
    foreach ($result as $row) {
      $temas[$row['id']] = $row;
    }

  } else {
    $temas = false;
  }
  return $temas;
}
 ?>
