<?php include(HTML_DIR . 'overall/header.php'); ?>

<body>
<section class="engine"><a rel="nofollow" href="#"><?php echo APP_TITLE ?></a></section>

<?php include(HTML_DIR . '/overall/topnav.php'); ?>

<section class="mbr-section mbr-after-navbar">
<div class="mbr-section__container container mbr-section__container--isolated">

  <?php
  if(isset($_GET['success'])) {
    echo '<div class="alert alert-dismissible alert-success">
      <strong>Completado!</strong> el foro ha sido editado.
    </div>';
  }
  if(isset($_GET['error'])) {
    if($_GET['error'] == 1) {
      echo '<div class="alert alert-dismissible alert-danger">
        <strong>Error!</strong></strong> Debes completar todos los campos.
      </div>';
    } else {
      echo '<div class="alert alert-dismissible alert-danger">
        <strong>Error!</strong></strong> Indica la categoría.
      </div>';
    }
  }
  ?>

<div class="row container">
  <div class="pull-right">
    <div class="mbr-navbar__column"><ul class="mbr-navbar__items mbr-navbar__items--right mbr-buttons mbr-buttons--freeze mbr-buttons--right btn-inverse mbr-buttons--active"><li class="mbr-navbar__item">
         <a class="mbr-buttons__btn btn btn-danger" href="?view=configforos">GESTIONAR FOROS</a>
     </li></ul></div>
     <div class="mbr-navbar__column"><ul class="mbr-navbar__items mbr-navbar__items--right mbr-buttons mbr-buttons--freeze mbr-buttons--right btn-inverse mbr-buttons--active"><li class="mbr-navbar__item">
         <a class="mbr-buttons__btn btn btn-danger" href="?view=configforos&mode=add">CREAR FORO</a>
     </li></ul></div>
      <div class="mbr-navbar__column"><ul class="mbr-navbar__items mbr-navbar__items--right mbr-buttons mbr-buttons--freeze mbr-buttons--right btn-inverse mbr-buttons--active"><li class="mbr-navbar__item">
          <a class="mbr-buttons__btn btn btn-danger" href="?view=categorias">GESTIONAR CATEGORÍAS</a>
      </li></ul></div>
    </div>

    <ol class="breadcrumb">
      <li><a href="?view=index"><i class="fa fa-comments"></i> Foros</a></li>
    </ol>
</div>

<div class="row categorias_con_foros">
  <div class="col-sm-12">
      <div class="row titulo_categoria">Gestión de Foros</div>

      <div class="row cajas">
        <div class="col-md-12">
          <?php $id_foro = intval($_GET['id']);?>
          <form class="form-horizontal" action="?view=configforos&mode=edit&id=<?php echo $id_foro;?>" method="POST" enctype="application/x-www-form-urlencoded">
            <fieldset>
              <div class="form-group">
                <label for="inputEmail" class="col-lg-2 control-label">Nombre</label>
                <div class="col-lg-10">
                  <input type="text" class="form-control" maxlength="200" name="nombre" placeholder="Nombre para el foro"
                  value="<?php echo $fora[$id_foro]['nombre'];?>">
                </div>
              </div>
              <div class="form-group">
                <label for="inputEmail" class="col-lg-2 control-label">Descripción</label>
                <div class="col-lg-10">
                  <input type="text" class="form-control" maxlength="250" name="descripcion" placeholder="Descripción para el foro"
                  value="<?php echo $fora[$id_foro]['descripcion'];?>">
                </div>
              </div>
              <div class="form-group">
                <label class="col-lg-2 control-label">Estado</label>
                <div class="col-lg-10">
                  <div class="radio">
                    <label>
                      <input name="estado" id="estado1" value="1" <?php if ($fora[$id_foro]['estado'] == 1) {echo 'checked=""';}?> type="radio">
                      Abierto
                    </label>
                  </div>
                  <div class="radio">
                    <label>
                      <input name="estado" id="estado0" value="0" <?php if ($fora[$id_foro]['estado'] == 0) {echo 'checked=""';}?> type="radio">
                      Cerrado
                    </label>
                  </div>
                </div>
              </div>
              <div class="form-group">
                <label for="inputEmail" class="col-lg-2 control-label">Categoría</label>
                <div class="col-lg-10">
                  <select name="categoria">
                    <option value="0">Sin categoría</option>
                    <?php
                      if (false != $categories) {
                        foreach($categories as $categoria) {
                          if($categoria['id'] == $fora[$id_foro]['id_categoria']) {
                            echo '<option value="'. $categoria['id'] .'" selected>'.$categoria['nombre'].'</option>';
                          } else {
                            echo '<option value="'. $categoria['id'] .'">'.$categoria['nombre'].'</option>';
                          }
                        }
                      } else {
                        echo '<option value="0">Sin categoría</option>';
                      }

                    ?>

                  </select>
                </div>
              </div>
              <div class="form-group">
                <div class="col-lg-10 col-lg-offset-2">
                  <button type="reset" class="btn btn-default">Resetear</button>
                  <button type="submit" class="btn btn-primary">Editar</button>
                </div>
              </div>
            </fieldset>
          </form>
        </div>
      </div>
  </div>
</div>

</div>
</section>

<?php include(HTML_DIR . 'overall/footer.php'); ?>

</body>
</html>
