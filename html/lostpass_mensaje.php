<?php include(HTML_DIR . 'overall/header.php'); ?>

<body>

    <?php include(HTML_DIR . 'overall/topnav.php'); ?>

    <section class="mbr-section mbr-after-navbar">
        <div class="mbr-section__container container mbr-section__container--isolated">
            <div class="alert alert-dismissible alert-success">
                <a data-toggle="modal" data-target="#Login">Inicia Sesión</a> con esta contraseña
                <?= $prenewpass;?>
               
            </div>
        </div>
    </section>


    <?php include(HTML_DIR . 'overall/footer.php'); ?>

</body>
</html>
