<?php include(HTML_DIR . 'overall/header.php');?>

<body>

<?php include(HTML_DIR . 'overall/topnav.php');?>

<section class="mbr-section mbr-after-navbar" id="content1-10">
    <div class="mbr-section__container container mbr-section__container--isolated">
        <div class="row">
            <div class="mbr-article mbr-article--wysiwyg col-sm-8 col-sm-offset-2">
            <div class="alert alert-dismissible alert-warning">
                <button type="button" class="close" data-dismiss="alert">&times;</button>
                <h4>¡Inicia Sesión!</h4>
                <p>Debes estar logueado para activar tu cuenta. <a data-toggle="modal" data-target="#Login" class="alert-link">Inicia Sesión</a>.</p>
          </div>
          </div>
        </div>
    </div>
</section>
    
 <?php include(HTML_DIR . 'overall/footer.php');?>

</body>
</html>


