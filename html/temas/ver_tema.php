<?php include(HTML_DIR . 'overall/header.php'); ?>

<body>
<section class="engine"><a rel="nofollow" href="#"><?php echo APP_TITLE ?></a></section>

<?php include(HTML_DIR . '/overall/topnav.php'); ?>

<section class="mbr-section mbr-after-navbar">
<div class="mbr-section__container container mbr-section__container--isolated">

<div class="row container">
  <div class="pull-right">
    <?php
      $permisosOdueno = ($users[$_SESSION['app_id']]['permisos'] > 0 || $_SESSION['app_id'] == $tema['id_creador']);
      if ($permisosOdueno) {
          ?>
        <?php if ($tema['estado'] == 1) {
              ?>
        <div class="mbr-navbar__column"><ul class="mbr-navbar__items mbr-navbar__items--right mbr-buttons mbr-buttons--freeze mbr-buttons--right btn-inverse mbr-buttons--active"><li class="mbr-navbar__item">
             <a class="mbr-buttons__btn btn btn-danger" href="?view=temas&mode=close&id=<?= $_GET['id']; ?>&id_foro=<?= $tema['id_foro']; ?>&estado=0">CERRAR</a>
         </li></ul></div>
        <!--/div-->
        <?php
          } ?>
      <div class="mbr-navbar__column"><ul class="mbr-navbar__items mbr-navbar__items--right mbr-buttons mbr-buttons--freeze mbr-buttons--right btn-inverse mbr-buttons--active"><li class="mbr-navbar__item">
           <a class="mbr-buttons__btn btn btn-danger" href="?view=temas&mode=delete&id=<?= $_GET['id']; ?>&id_foro=<?= $tema['id_foro']; ?>&estado=0">BORRAR</a>
       </li></ul></div>
      <?php
      } ?>


    <?php if ($tema['estado'] == 1) {
          ?>
      <div class="mbr-navbar__column"><ul class="mbr-navbar__items mbr-navbar__items--right mbr-buttons mbr-buttons--freeze mbr-buttons--right btn-inverse mbr-buttons--active"><li class="mbr-navbar__item">
           <a class="mbr-buttons__btn btn btn-danger" href="?view=temas&mode=responder&id=<?= $_GET['id']; ?>&id_foro=<?= $tema['id_foro']; ?>">RESPONDER</a>
       </li></ul></div>

      <?php
      } elseif ($permisosOdueno && $tema['estado'] == 0) {
          ?>
        <div class="mbr-navbar__column"><ul class="mbr-navbar__items mbr-navbar__items--right mbr-buttons mbr-buttons--freeze mbr-buttons--right btn-inverse mbr-buttons--active"><li class="mbr-navbar__item">
             <a class="mbr-buttons__btn btn btn-danger" href="?view=temas&mode=close&id=<?= $_GET['id']; ?>&id_foro=<?= $tema['id_foro']; ?>&estado=1">ABRIR</a>
         </li></ul></div>
      <?php
      } ?>
      </div>
    <ol class="breadcrumb">
      <li><a href="?view=foros&id=<?= $tema['id_foro']?>"><i class="fa fa-user"></i> Foro </a></li>
    </ol>
</div>

<div class="row categorias_con_foros">
  <div class="col-sm-12">
      <div class="row titulo_categoria"><?= $tema['titulo'];?></div>
      <div class="row cajas">
        <div class="col-md-2">
          <center>

            <img src="views/app/images/users/<?php echo $users[$tema['id_creador']]['img']; ?>" class="thumbnail" height="120" />

            <strong><?php echo $users[$tema['id_creador']]['user']; ?></strong>
            <img src="views/app/images/<?php echo GetUserStatus($users[$tema['id_creador']]['ultima_conexion']); ?>" />

            <br />
            <b style="color: green;">**<?php echo $users[$tema['id_creador']]['rango']; ?>**</b>
            <br /><br />
          </center>

            <ul style="list-style:none; padding-left: 4px;">
              <li><b><?php echo $users[intval($tema['id_creador'])]['mensajes']; ?></b> mensajes</li>
              <li><b><?php echo $users[intval($tema['id_creador'])]['edad']; ?></b> años</li>
              <li>Registrado el <b><?php echo $users[$tema['id_creador']]['fecha_reg']; ?></b></li>
            </ul>


        </div>
        <div class="col-md-10">
          <blockquote>
            <?php echo BBcode($tema['contenido']); ?>
          </blockquote>

          <?php if ($permisosOdueno) {
          ?>
            <hr />
            <a href="index.php?view=temas&mode=edit&id=<?= $_GET['id']; ?>&id_foro=<?= $_GET['id_foro']; ?>" class="btn btn-primary">Editar mensaje</a>
          <!--/div-->
          <?php
      } ?>
          <hr />
          <p>
            <?php echo BBcode($users[$tema['id_creador']]['firma']); ?>
          </p>
        </div>
      </div>
  </div>
  <?php
  if (false != $responses) {
      foreach ($responses as $respuesta) {
          ?>

    <div class="col-sm-12">
        <div class="row cajas">
          <div class="col-md-2">
            <center>

              <img src="views/app/images/users/<?php echo $users[$respuesta['id_creador']]['img']; ?>" class="thumbnail" height="120" />

              <strong><?php echo $users[$respuesta['id_creador']]['user']; ?></strong>
              <img src="views/app/images/<?php echo GetUserStatus($users[$respuesta['id_creador']]['ultima_conexion']); ?>" />

              <br />
              <b style="color: green;">**<?php echo $users[$respuesta['id_creador']]['rango']; ?>**</b>
              <br /><br />
            </center>

              <ul style="list-style:none; padding-left: 4px;">
                <li><b><?php echo $users[intval($respuesta['id_creador'])]['mensajes']; ?></b> mensajes</li>
                <li><b><?php echo $users[intval($respuesta['id_creador'])]['edad']; ?></b> años</li>
                <li>Registrado el <b><?php echo $users[$respuesta['id_creador']]['fecha_reg']; ?></b></li>
              </ul>


          </div>
          <div class="col-md-10">
            <blockquote>
              <?= BBCode($respuesta['contenido']); ?>
            </blockquote>
            <hr />
            <p>
              <?php echo BBcode($users[$respuesta['id_creador']]['firma']); ?>
            </p>
          </div>
        </div>
    </div>
  <?php
      }//cierra el for?>

<?php
  } //cierra if?>
</div>

</div>
</section>

<?php include(HTML_DIR . 'overall/footer.php'); ?>

</body>
</html>
