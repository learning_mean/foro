<?php include(HTML_DIR . 'overall/header.php'); ?>

<body>

    <?php include(HTML_DIR . 'overall/topnav.php'); ?>

    <section class="mbr-section mbr-after-navbar">
        <div class="mbr-section__container container mbr-section__container--isolated">

            <?php
            if (isset($_GET['success'])) {
                echo '<div class="alert alert-dismissible alert-success">
      <strong>¡Activado!</strong> tu usuario ha sido activado correctamente.
    </div>';
            }
            if (isset($_GET['error'])) {
                echo '<div class="alert alert-dismissible alert-danger">
      <strong>¡Error!</strong></strong> no se ha podido activar tu usuario.
    </div>';
            }
            ?>

            <div class="row container">
                <?php
                if (isset($_SESSION['app_id']) && $users[$_SESSION['app_id']]['permisos'] == 2) {
                    echo ' <div class="pull-right">
            <div class="mbr-navbar__column"><ul class="mbr-navbar__items mbr-navbar__items--right mbr-buttons mbr-buttons--freeze mbr-buttons--right btn-inverse mbr-buttons--active"><li class="mbr-navbar__item">
              <a class="mbr-buttons__btn btn btn-danger" href="?view=configforos">GESTIONAR FOROS</a>
            </li></ul></div>
            <div class="mbr-navbar__column"><ul class="mbr-navbar__items mbr-navbar__items--right mbr-buttons mbr-buttons--freeze mbr-buttons--right btn-inverse mbr-buttons--active"><li class="mbr-navbar__item">
              <a class="mbr-buttons__btn btn btn-danger" href="?view=categorias">GESTIONAR CATEGORÍAS</a>
            </li></ul></div>
          </div>
          ';
                }
                ?>
                <ol class="breadcrumb">
                    <li><a href="?view=index"><i class="fa fa-home"></i> Inicio</a></li>
                </ol>
            </div>
            <?php
            if (false != $categories) {
                foreach ($categories as $categoria) {
                    ?>
                    <div class="row categorias_con_foros">
                        <div class="col-sm-12">
                            <div class="row titulo_categoria"><?= $categoria['nombre'] ?></div>
                            <?php
                            $foros = array();
                            foreach ($fora as $forum) {
                                if ($forum['id_categoria'] == $categoria['id']) {
                                    $foros[] = $forum;
                                }
                            }
                            if (empty($foros)) {
                                echo '<div class="row foros">
                              <div class="col-md-1" style="height:50px;line-height: 37px;">
                                  <img src="views/app/images/foro/foro_noleido_bloqueado.png">
                              </div>
                              <div class="col-md-7 puntitos" style="padding-left: 0px;">
                                  No existe ningún foro en esta categoría
                              </div>
                          </div>';
                            } else {
                                foreach ($foros as $foro) {
                                  $id_foro = $foro['id'];
                                    ?>
                                    <div class="row foros">
                                        <div class="col-md-1" style="height:50px;line-height: 37px;">
                                            <?php
                                            if ($foro['estado'] == 0) {
                                                echo '<img src="views/app/images/foro/foro_noleido_bloqueado.png" />';
                                            } else {
                                                echo '<img src="views/app/images/foro/foro_leido.png" />';
                                            }
                                            ?>
                                        </div>
                                        <div class="col-md-7 puntitos" style="padding-left: 0px;">
                                            <a href="foros/<?= UrlAmigable($foro['id'], $foro['nombre'])?>"><?= $foro['nombre'] ?></a><br />
                                            <?= $foro['descripcion'] ?>
                                        </div>
                                        <div class="col-md-2 left_border" style="text-align: center;font-weight: bold;">
                <?= number_format($foro['numero_temas'], 0, ',', '.') ?> Temas<br />
                <?= number_format($foro['numero_mensajes'], 0, ',', '.') ?> Mensajes
                                        </div>
                                        <div class="col-md-2 left_border puntitos" style="line-height: 37px;">
                                        <?php echo empty($foro['ultimo_tema']) ? '<a href="#">No hay temas</a>' :
                                          '<a href="temas/'.UrlAmigable($foro['id_ultimo_tema'],$foro['ultimo_tema'], $id_foro).'">'.$foro['ultimo_tema'].'</a>';?>
                                        </div>
                                    </div>
            <?php }
        } ?>


                        </div>

                    </div>

        <?php
    }
} else {
    ?>
                <div class="row categorias_con_foros">
                    <div class="col-sm-12">
                        <div class="row titulo_categoria"><?= APP_TITLE ?></div>
                        <div class="row foros">
                            <div class="col-md-1" style="height:50px;line-height: 37px;">
                                <img src="views/app/images/foro/foro_leido.png" />
                            </div>
                            <div class="col-md-7 puntitos" style="padding-left: 0px;">
                                <a href="#">No hay categoria</a><br />
                                No existe la categoría

                            </div>
                        </div>

                    </div>

                </div>



            </div>

<?php }
?>

    </section>


<?php include(HTML_DIR . 'overall/footer.php'); ?>

</body>
</html>
