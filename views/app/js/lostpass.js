function goLostpass() {
  var connect, form, result, email;
  email = __('email_lostpass').value;

  if (check(email)) {
    var patron = /^[a-zA-Z0-9._-]+@[a-zA-Z0-9._-]+\.[a-zA-Z]{2,4}$/;
    if (patron.test(email)) {
      form = 'email=' + email;
      connect = window.XMLHttpRequest ? new XMLHttpRequest() : new ActiveXObject('Microsoft.XMLHTTP');
      connect.onreadystatechange = function() {
        if(connect.readyState == 4 && connect.status == 200) {
          if(connect.responseText == 1) {
            result = '<div class="alert alert-dismissible alert-success">';
            result += '<h4>¡Correo Enviado!</h4>';
            result += '<p>Sigue las instrucciones contenidas en el email</p>';
            result += '</div>';
            __('_AJAX_LOSTPASS_').innerHTML = result;
            location.reload();
          } else {
            __('_AJAX_LOSTPASS_').innerHTML = connect.responseText;
          }
        } else if(connect.readyState != 4) {
          result = '<div class="alert alert-dismissible alert-warning">';
          result += '<button type="button" class="close" data-dismiss="alert">x</button>';
          result += '<h4>Enviando correo...</h4>';
          result += '<p>El envio se realizará en breves momentos</p>';
          result += '</div>';
          __('_AJAX_LOSTPASS_').innerHTML = result;
        }
      };
      connect.open('POST','ajax.php?mode=lostpass',true);
      connect.setRequestHeader('Content-Type','application/x-www-form-urlencoded');
      connect.send(form);
    } else {
      result = '<div class="alert alert-dismissible alert-danger">';
      result += '<button type="button" class="close" data-dismiss="alert">x</button>';
      result += '<h4>Email Inválido</h4>';
      result += '<p>Comprueba que tu dirección de correo esté formada correctamente</p>';
      result += '</div>';
      __('_AJAX_LOSTPASS_').innerHTML = result;
    }

  } else {
    result = '<div class="alert alert-dismissible alert-danger">';
    result += '<button type="button" class="close" data-dismiss="alert">x</button>';
    result += '<h4>Campo email vacío</h4>';
    result += '<p>Por favor ingresa una dirección válida</p>';
    result += '</div>';
    __('_AJAX_LOSTPASS_').innerHTML = result;
  }
}

function runScriptLostpass(e) {
  if(e.keyCode === 13) {
    goLostpass();
  }
}
