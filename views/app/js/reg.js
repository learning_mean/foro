function goReg() {
  var connect, form, result, user, pass, email,
  tyc, confirmacion;
  user = __('user_reg').value;
  email = __('email_reg').value;
  pass = __('pass_reg').value;
  confirmacion = __('pass_reg_dos').value;
  tyc = __('tyc_reg').checked ? true : false;

  if (tyc) {
    if (check(user, email, confirmacion)) { //verifica que NO estén vacíos
      if (pass === confirmacion) {
        form = 'user=' + user + '&pass=' + pass + '&email=' + email;
        //determina si el navegador es ie antiguo, version 5 ó 6
        connect = window.XMLHttpRequest ? new XMLHttpRequest() : new ActiveXObject('Microsoft.XMLHTTP');
        connect.onreadystatechange = function() {
          if(connect.readyState === 4 && connect.status === 200) {
            if((connect.responseText == 1)) {
              result = '<div class="alert alert-dismissible alert-success">';
              result += '<h4>Registrado con éxito</h4>';
              result += '<p><strong>Estamos redireccionándote...</strong></p>';
              result += '</div>';
              __('_AJAX_REG_').innerHTML = result;
              location.reload();
            } else {
              __('_AJAX_REG_').innerHTML = connect.responseText;
            }
          } else if(connect.readyState !== 4) {
            result = '<div class="alert alert-dismissible alert-warning">';
            result += '<button type="button" class="close" data-dismiss="alert">&times;</button>';
            result += '<h4>Procesando...</h4>';
            result += '<p><strong>Estamos intentando registrarte....</strong></p>';
            result += '</div>';
            __('_AJAX_REG_').innerHTML = result;
          }
        };
        connect.open('POST','ajax.php?mode=reg',true);
        connect.setRequestHeader('Content-Type','application/x-www-form-urlencoded');
        connect.send(form);
      } else {
        result = '<div class="alert alert-dismissible alert-danger">';
        result += '<button type="button" class="close" data-dismiss="alert">&times;</button>';
        result += '<strong>Error en Contraseñas.</strong> Las contraseñas deben coincidir.';
        result += '</div>';
        __('_AJAX_REG_').innerHTML = result;
      }

    } else {
      result = '<div class="alert alert-dismissible alert-danger">';
      result += '<button type="button" class="close" data-dismiss="alert">&times;</button>';
      result += '<strong>Campos Vacíos. </strong> Debes rellenar todos los campos.';
      result += '</div>';
      __('_AJAX_REG_').innerHTML = result;
    }

  } else {
    result = '<div class="alert alert-dismissible alert-danger">';
    result += '<button type="button" class="close" data-dismiss="alert">&times;</button>';
    result += '<h4>Terminos y Condiciones</h4>';
    result += '<p><strong>Debes aceptar los términos y condiciones del servicio</strong></p>';
    result += '</div>';
    __('_AJAX_REG_').innerHTML = result;
  }
}

function runScriptReg(e) {
  try {
    if(e.keyCode === 13) {
      goReg();
    }
  } catch (e) {
    console.log("Hubo un problema con el keycode " + e.message + ' ' + e.keyCode);
  }
}
