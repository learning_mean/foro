<?php
if($_POST) {
  require('core/core.php');
  switch (isset($_GET['mode']) ? $_GET['mode'] : null) {
    case 'login':
      include('core/bin/ajax/goLogin.php');
      break;
    case 'reg':
      include('core/bin/ajax/goReg.php');
      break;
    case 'lostpass':
      include('core/bin/ajax/goLostpass.php');
      break;
    default:
      header('location: index.php');
      break;
  }
} else {
  header('location: index.php');
}

?>
